import API from "./API.js";
import Modal from "./Modal.js";
import {VisitDentist, VisitCardiologist, VisitTherapist} from "./Visits.js";
import {Form} from "./Forms.js";
import Loader from "./Loader.js";
import {baseURL} from "./Config.js";

export const api = new API(baseURL);
export const board = document.getElementById("board");
const loginModal = new Modal(document.getElementById("loginModal"));
export const createModal = new Modal(document.getElementById("createModal"));
export const editModal = new Modal(document.getElementById("editModal"));

const boardLoader = new Loader("spinner-border text-primary mx-auto", board);
const loginBtn = document.querySelector(".header__login-btn");
const headerCreateBtn = document.querySelector(".header__create-btn");
const loginForm = loginModal.modal.querySelector("form");
const filterForm = document.getElementById("filterForm");

loginBtn.addEventListener("click", loginBtnClickHandler);
headerCreateBtn.addEventListener("click", headerCreateBtnClickHandler);
loginForm.addEventListener("submit", onLoginFormSubmit);
filterForm.addEventListener("submit", onFilterFormSubmit);


checkLogin().catch(error => {
    boardLoader.hide();
    alert(error);
});


async function checkLogin() {
    if (localStorage.getItem("login")) {
        boardLoader.show();
        handleLogin();
        api.token = localStorage.getItem("token");
        const visits = await api.request("GET");
        renderCards(visits, true);
    }
}

function handleLogin() {
    loginBtn.textContent = "Sign Out";
    loginBtn.classList.add("logined");
    loginBtn.classList.replace("btn-outline-primary", "btn-outline-danger");
    headerCreateBtn.classList.remove("d-none");
    filterForm.classList.remove("d-none");
}

function handleLogout() {
    localStorage.clear();
    loginBtn.classList.remove("logined");
    headerCreateBtn.classList.add("d-none");
    loginBtn.classList.replace("btn-outline-danger", "btn-outline-primary");
    loginBtn.textContent = "Sign In";
    filterForm.classList.add("d-none");
    const noItemsMsg = createMsg("No items have been added", "text mx-auto");
    board.textContent = "";
    board.append(noItemsMsg);
}

export function renderCards(data, clearAll = false) {
    if (!data.length) {
        const noItemsMsg = createMsg("No items have been added", "text mx-auto");
        board.textContent = "";
        board.append(noItemsMsg);
    }
    if (data.length > 0 && clearAll) board.textContent = "";
    data.forEach(({id, content}) => {
        const visitData = {id, ...content};
        const {doctor} = visitData;
        let card;
        if (doctor === "Dentist") {
            card = new VisitDentist(visitData);
        } else if (doctor === "Cardiologist") {
            card = new VisitCardiologist(visitData);
        } else if (doctor === "Therapist") {
            card = new VisitTherapist(visitData);
        }
        if (card) card.render();
    })
}

async function onLoginFormSubmit(event) {
    try {
        event.preventDefault();
        const [emailField, passField] = this.elements;
        const token = await api.login({
            "email": emailField.value,
            "password": passField.value
        });
        boardLoader.show();
        localStorage.setItem("login", "true");
        localStorage.setItem("token", token);
        clearAlert(loginForm);
        loginModal.hide();
        handleLogin();
        api.token = token;
        const visits = await api.request("GET");
        renderCards(visits, true);
    } catch (error) {
        boardLoader.hide();
        clearAlert(loginForm);
        const alertMsg = createMsg(error, "alert alert-danger");
        loginForm.prepend(alertMsg);
    }
}

async function onFilterFormSubmit(event) {
    try {
        event.preventDefault();
        boardLoader.show();
        const visits = await api.request("GET");
        const result = visits.filter(visit => checkVisitMatch(visit, this.title, this.urgency, this.status));
        if (!result.length) {
            const noMatchesMsg = createMsg("No matches", "text mx-auto");
            board.textContent = "";
            board.append(noMatchesMsg);
        } else renderCards(result, true);
    } catch (error) {
        boardLoader.hide();
        alert(error);
    }
}

function checkVisitMatch({content: {fullName: visitName, doctor: visitDoctor, urgency: visitUrgency, status: visitStatus = "Open"}}, {value: searchTitle}, {value: searchUrgency}, {value: searchStatus}) {
    visitName = visitName.trim().toLowerCase();
    visitDoctor = visitDoctor.trim().toLowerCase();
    searchTitle = searchTitle.trim().toLowerCase();
    return ((visitName.includes(searchTitle) || visitDoctor.includes(searchTitle)) && visitStatus.includes(searchStatus) && visitUrgency.includes(searchUrgency));
}

function headerCreateBtnClickHandler() {
    const createForm = new Form().create();
    createModal.insert(createForm);
    createModal.show();
}

function loginBtnClickHandler() {
    if (!this.classList.contains("logined")) {
        loginModal.show();
    } else handleLogout();
}

function createMsg(textContent, className) {
    const msg = document.createElement("div");
    msg.className = className;
    msg.textContent = textContent;
    return msg;
}

function clearAlert(element) {
    const foundAlert = element.querySelector(".alert");
    if (foundAlert) foundAlert.remove();
}