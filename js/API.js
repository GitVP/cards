export default class API {
    constructor(url) {
        this.url = url;
        this.token = "";
    }

    async login(loginData) {
        const response = await fetch(`${this.url}/login`, {
            method: "POST",
            body: JSON.stringify(loginData),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });
        const value = await response.text();
        if (!response.ok) {
            throw new Error(value);
        }
        return value;
    }

    async request(method, queryNumber = "", data) {
        const response = await fetch(`${this.url}/${queryNumber}`, {
            method: method,
            body: JSON.stringify(data),
            headers: {
                Authorization: `Bearer ${this.token}`,
                "Content-type": "application/json; charset=UTF-8",
            },
        });
        const value = method === "DELETE" ? await response.text() : await response.json();
        if (!response.ok) {
            throw new Error(value);
        }
        return value;
    }
}