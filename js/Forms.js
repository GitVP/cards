import {Input, Select, Textarea, Button, InputSubmit} from "./Components.js"
import {api, editModal, createModal, renderCards} from "./main.js";

export class Form {
    create() {
        const form = document.createElement("form");
        form.id = "visit-form";
        form.className = "form-center d-flex  flex-column";
        form.setAttribute("action", "#");

        const docSelect = new Select(["Cardiologist", "Dentist", "Therapist"], "doctor", "Choose doctor:").create();
        form.append(docSelect);
        const commonInputs = document.createElement("div");
        commonInputs.id = "commonWrapper";
        commonInputs.className = "d-none";
        commonInputs.append(...this._createCommonInputs());
        const specInputs = document.createElement("div");
        const buttons = this._createButtons();
        buttons.className = "d-none";
        buttons.id = "buttonWrapper";
        specInputs.id = "specWrapper"
        docSelect.addEventListener("change", () => {
            const {value: doctor} = docSelect;
            specInputs.textContent = "";
            this._chooseDoctor(doctor, specInputs, commonInputs, buttons);
        });
        form.append(commonInputs, specInputs, buttons);
        form.addEventListener("submit", this._onFormSubmit);
        return form;
    }

    createFilled(visit) {
        const form = this.create();
        const {id, doctor} = visit;
        form.setAttribute("data-action", "edit");
        form.setAttribute("data-id", id);
        const specInputs = form.querySelector("#specWrapper");
        const commonInputs = form.querySelector("#commonWrapper");
        const buttons = form.querySelector("#buttonWrapper");
        buttons.querySelector("#form-submit-btn").value = "Save change";
        this._chooseDoctor(doctor, specInputs, commonInputs, buttons);
        for (const visitKey in visit) {
            if (form.hasOwnProperty(visitKey)) form[visitKey].value = visit[visitKey];
        }
        const status = new Select(["Open", "Done"], "status").create();
        status.value = "Open";
        form.prepend(status);

        form.setAttribute("data-action", "edit");
        form.setAttribute("data-id", id);
        return form;
    }

    _createCommonInputs() {
        const purposeField = new Input("purpose", "text").create();
        const descriptionField = new Textarea().create();
        const urgencyField = new Select(["Low", "Middle", "High"], "urgency", "Choose urgency:").create();
        const fullNameField = new Input("fullName", "text").create();
        return [fullNameField, purposeField, descriptionField, urgencyField];
    }

    _createCardioInputs() {
        const bpField = new Input("bp", "text").create();
        const bmiField = new Input("bmi", "number").create();
        const diseasesField = new Input("diseases", "text").create();
        const ageField = new Input("age", "number").create();
        return [bpField, bmiField, diseasesField, ageField];
    }

    _createDentistInputs() {
        return [new Input("lastVisitDate", "date").create()];
    }

    _createTherapistInputs() {
        return [new Input("age", "number").create()];
    }

    _chooseDoctor(doctor, specInputs, commonInput, buttons){
        if (doctor === "Cardiologist") {
            this._showMoreElements(commonInput, buttons);
            specInputs.append(...this._createCardioInputs());
        } else if (doctor === "Dentist") {
            this._showMoreElements(commonInput, buttons);
            specInputs.append(...this._createDentistInputs());
        } else if (doctor === "Therapist") {
            this._showMoreElements(commonInput, buttons);
            specInputs.append(...this._createTherapistInputs());
        }
    }
    _createButtons(){
        const buttons = document.createElement("div");
        const btnClose = new Button("button", "Close", "btn btn-secondary", "modal").create();
        const btnCreate = new InputSubmit("submit", "Create Visit").create();

        buttons.append(btnCreate, btnClose);
        return buttons;
    }
    _showMoreElements(commonInput, buttons){
        if(commonInput.classList.contains("d-none")) {
            commonInput.className = "";
            buttons.className = "d-flex justify-content-between mt-3";
        }
    }
    async _onFormSubmit(event) {
        try {
            event.preventDefault();
            const visitData = {};
            const formData = new FormData(this);
            formData.forEach((value, key) => {visitData[key] = value});
            if (this.getAttribute("data-action") === "edit") {
                const editId = this.getAttribute("data-id");
                await api.request("PUT", editId, visitData);
                editModal.hide();
                const visits = await api.request("GET");
                renderCards(visits, true);
            } else {
                visitData.status = "Open";
                const createdVisit = await api.request("POST", "", visitData);
                createModal.hide();
                renderCards([createdVisit]);
            }
        } catch (error) {
            alert(error);
        }
    }
}