import {api, editModal, board} from "./main.js";
import {Form} from "./Forms.js";

class Visit {
    constructor(id, purpose, description, urgency, fullName, doctor, status) {
        this.id = id;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.fullName = fullName;
        this.doctor = doctor;
        this.status = status;
        this.card = this._createCard();
    }

    render() {
        board.append(this.card);
    }

    _expand() {
        const infoList = this._createCardInfoList();
        this.card.append(infoList);
        const {bottom: boardBottom} = board.getBoundingClientRect();
        const {bottom: cardBottom} = this.card.getBoundingClientRect();
        if (boardBottom < cardBottom) {
            board.style.height = board.offsetHeight + cardBottom - boardBottom + 20 + "px";
        }
    }

    _createCardInfoList() {
        const infoList = document.createElement("ul");
        infoList.className = "list-group list-group-flush";
        for (const prop in this) {
            if (prop === "id" || prop === "doctor" || prop === "fullName" || prop === "card") continue;
            const info = this._createCardInfoItem(prop);
            infoList.append(info);
        }
        return infoList;
    }

    _createCardInfoItem(prop) {
        const info = document.createElement("li");
        info.className = "list-group-item";
        info.textContent = `${prop}: ${this[prop]}`;
        return info;
    }

    _createCard() {
        const cardWrapper = this._createCardWrapper();
        const cardBody = this._createCardBody();
        const cardTitle = this._createCardTitle({textContent: this.fullName});
        const cardDoctor = this._createCardDoctor({textContent: this.doctor});
        const btnDel = this._createCardBtn("btn-close card__del-btn");
        const btnEdit = this._createCardBtn("card__edit-btn");
        const btnShowMore = this._createCardBtn("btn btn-info d-block mx-auto", "Show More");

        btnDel.addEventListener("click", () => {
            api.request("DELETE", this.id)
                .then(() => this.card.remove())
                .catch(error => alert(error))
        });

        btnEdit.addEventListener("click", () => {
            const editForm = new Form().createFilled(this);
            editModal.insert(editForm);
            editModal.show();
        });

        btnShowMore.addEventListener("click", () => {
            btnShowMore.remove();
            this._expand();
        });

        cardBody.append(cardTitle, cardDoctor, btnDel, btnEdit, btnShowMore);
        cardWrapper.append(cardBody);
        cardWrapper.addEventListener("mousedown", this._onCardMouseDown);
        cardWrapper.addEventListener("dragstart", () => false);
        return cardWrapper;
    }

    _createCardWrapper() {
        const cardWrapper = document.createElement("div");
        cardWrapper.className = "card bg-light user-select-none board__card";
        return cardWrapper;
    }

    _createCardBody() {
        const cardBody = document.createElement("div");
        cardBody.className = "card-body";
        return cardBody;
    }

    _createCardTitle({textContent}) {
        const cardTitle = document.createElement("h5");
        cardTitle.className = "card-title";
        cardTitle.textContent = textContent;
        return cardTitle;
    }

    _createCardDoctor({textContent}) {
        const cardDoctor = document.createElement("h6");
        cardDoctor.className = "card-subtitle mb-2 text-muted";
        cardDoctor.textContent = textContent;
        return cardDoctor;
    }

    _createCardBtn(className, textContent = "") {
        const btn = document.createElement("button");
        btn.className = className;
        btn.textContent = textContent;
        return btn;
    }

    _onCardMouseDown(event) {
        if (event.target.tagName === "BUTTON") return;
        const card = this;
        const {left: cardLeft, top: cardTop} = card.getBoundingClientRect();
        const {boardLeft, boardRight, boardTop, boardBottom} = getBoardBound();
        const shiftX = event.clientX - cardLeft + 20;
        const shiftY = event.clientY - cardTop + 20;
        card.style.position = "absolute";
        card.style.zIndex = "1000";
        moveTo(event.pageX, event.pageY);
        document.addEventListener("mousemove", onMouseMove);
        document.addEventListener("mouseup", () => document.removeEventListener("mousemove", onMouseMove));

        function getBoardBound() {
            const bound = board.getBoundingClientRect();
            return {
                boardLeft: bound.left,
                boardRight: bound.right - card.offsetWidth - 40,
                boardTop: bound.top,
                boardBottom: bound.bottom - card.offsetHeight - 40 + window.scrollY
            }
        }

        function onMouseMove(event) {
            moveTo(event.pageX, event.pageY);
        }

        function moveTo(pageX, pageY) {
            const x = pageX - shiftX;
            const y = pageY - shiftY;
            if (x >= boardLeft && x <= boardRight) card.style.left = `${x}px`;
            if (y >= boardTop && y <= boardBottom) card.style.top = `${y}px`;
        }
    }
}

export class VisitCardiologist extends Visit {
    constructor({id, purpose, description, urgency, fullName, doctor, status, bp, bmi, diseases, age}) {
        super(id, purpose, description, urgency, fullName, doctor, status);
        this.bp = bp;
        this.bmi = bmi;
        this.diseases = diseases;
        this.age = age;
    }
}

export class VisitDentist extends Visit {
    constructor({id, purpose, description, urgency, fullName, doctor, status, lastVisitDate}) {
        super(id, purpose, description, urgency, fullName, doctor, status);
        this.lastVisitDate = lastVisitDate;
    }
}

export class VisitTherapist extends Visit {
    constructor({id, purpose, description, urgency, fullName, doctor, status, age}) {
        super(id, purpose, description, urgency, fullName, doctor, status);
        this.age = age;
    }
}