export default class Modal {
    constructor(modal) {
        this.modal = modal;
        this.bootstrapModal = new bootstrap.Modal(modal, {
            keyboard: false
        });
        this.modal.addEventListener("shown.bs.modal", function () {
            const firstInput = this.querySelector("input");
            if (firstInput) firstInput.focus();
        });
    }

    insert(form) {
        const modalBody = this.modal.querySelector(".modal-body");
        modalBody.textContent = "";
        modalBody.append(form);
    }

    show() {
        this.bootstrapModal.show();
    }

    hide() {
        this.bootstrapModal.hide();
    }
}