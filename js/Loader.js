export default class Loader {
    constructor(className, parentElem) {
        this.className = className;
        this.parentElem = parentElem;
        this.loader = this._create();
    }

    show(clearParent = true) {
        if (clearParent) this.parentElem.textContent = "";
        this.parentElem.append(this.loader);
    }

    hide() {
        if (this.parentElem.contains(this.loader)) this.loader.remove();
    }

    _create() {
        const loader = document.createElement("div");
        loader.className = this.className;
        return loader;
    }
}