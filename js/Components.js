export class Input {
    constructor(name, type) {
        this.name = name;
        this.type = type;
    }

    create() {
        const input = document.createElement("input");
        input.setAttribute("type", this.type);
        input.setAttribute("name", this.name);
        input.setAttribute("placeholder", this.name);
        input.setAttribute("required", "required");
        input.className = "form-control mb-2";
        return input;
    }
}

export class Select {
    constructor(options, name, title) {
        this.options = options;
        this.name = name;
        this.title = title;
    }

    create() {
        const inputSelect = document.createElement("select");
        inputSelect.setAttribute("name", this.name);
        inputSelect.setAttribute("required", "required");
        inputSelect.className = "form-select mb-2";
        inputSelect.append(new Option(this.title, "", true, true));
        this.options.forEach((key) => {
            inputSelect.append(new Option(key));
        })
        const inputTitle = inputSelect.options[0];
        inputTitle.setAttribute("disabled", "disabled");
        inputTitle.setAttribute("hidden", "hidden");
        return inputSelect;
    }
}

export class Textarea {
    constructor(description) {
        this.description = description;
    }

    create() {
        const textarea = document.createElement("textarea");
        textarea.setAttribute("name", "description");
        textarea.setAttribute("placeholder", "description");
        textarea.className = "form-control mb-2";
        if (this.description) {
            textarea.value = this.description;
        }
        return textarea;
    }
}

export class Button {
    constructor(type, value, classList, attr) {
        this.type = type;
        this.value = value;
        this.classList = classList;
        this.attr = attr;
    }

    create() {
        const btn = document.createElement("button");
        btn.className = this.classList;
        btn.setAttribute("type", this.type);
        btn.setAttribute("data-bs-dismiss", this.attr);
        btn.textContent = this.value;
        return btn;
    }
}
export class InputSubmit {
    constructor(type, value) {
        this.type = type;
        this.value = value;
    }

    create() {
        const inputSub = document.createElement("input");
        inputSub.setAttribute("type", this.type);
        inputSub.setAttribute("value", this.value);
        inputSub.className = "btn btn-primary";
        inputSub.id = "form-submit-btn";
        return inputSub;
    }
}

